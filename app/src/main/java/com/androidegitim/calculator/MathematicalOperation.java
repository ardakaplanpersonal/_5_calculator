package com.androidegitim.calculator;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public enum MathematicalOperation {

    ADD,
    SUBSTRACTION,
    DIVIDE,
    MULTIPLY
}
