package com.androidegitim.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_textview_text)
    TextView textView;
    @BindView(R.id.main_textview_operation_sign)
    TextView operationSignTextView;


    private double firstValue = 0;
    private MathematicalOperation mathematicalOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    private void setText(String number) {

        String value = textView.getText().toString() + number;

        textView.setText(String.valueOf(Integer.valueOf(value)));

    }

    //    +++++++RAKAMLAR
    @OnClick(R.id.main_textview_0)
    public void _0() {

        setText("0");
    }

    @OnClick(R.id.main_textview_1)
    public void _1() {

        setText("1");
    }

    @OnClick(R.id.main_textview_2)
    public void _2() {

        setText("2");
    }

    @OnClick(R.id.main_textview_3)
    public void _3() {

        setText("3");
    }

    @OnClick(R.id.main_textview_4)
    public void _4() {

        setText("4");
    }

    @OnClick(R.id.main_textview_5)
    public void _5() {

        setText("5");
    }

    @OnClick(R.id.main_textview_6)
    public void _6() {

        setText("6");
    }

    @OnClick(R.id.main_textview_7)
    public void _7() {

        setText("7");
    }

    @OnClick(R.id.main_textview_8)
    public void _8() {

        setText("8");
    }

    @OnClick(R.id.main_textview_9)
    public void _9() {

        setText("9");
    }
    //    -------RAKAMLAR


    //    +++++++ISLEMLER
    @OnClick(R.id.main_textview_clear)
    public void clear() {

        textView.setText("0");

        operationSignTextView.setText("");

        firstValue = 0;

        mathematicalOperation = null;
    }

    @OnClick(R.id.main_textview_add)
    public void add() {

        operationSignTextView.setText(getFirstValue() + " +");

        mathematicalOperation = MathematicalOperation.ADD;
    }

    @OnClick(R.id.main_textview_subtraction)
    public void minus() {

        operationSignTextView.setText(getFirstValue() + " -");

        mathematicalOperation = MathematicalOperation.SUBSTRACTION;
    }

    @OnClick(R.id.main_textview_divide)
    public void divide() {

        operationSignTextView.setText(getFirstValue() + " /");

        mathematicalOperation = MathematicalOperation.DIVIDE;
    }

    @OnClick(R.id.main_textview_multiply)
    public void multiply() {

        operationSignTextView.setText(getFirstValue() + " x");

        mathematicalOperation = MathematicalOperation.MULTIPLY;
    }

    @OnClick(R.id.main_textview_equal)
    public void equal() {

        if (firstValue != 0 && mathematicalOperation != null) {

            double sum = 0;

            if (mathematicalOperation == MathematicalOperation.ADD) {

                sum = firstValue + Double.valueOf(textView.getText().toString());


            } else if (mathematicalOperation == MathematicalOperation.SUBSTRACTION) {

                sum = firstValue - Double.valueOf(textView.getText().toString());

            } else if (mathematicalOperation == MathematicalOperation.MULTIPLY) {

                sum = firstValue * Double.valueOf(textView.getText().toString());

            } else if (mathematicalOperation == MathematicalOperation.DIVIDE) {

                sum = firstValue / Double.valueOf(textView.getText().toString());
            }

            operationSignTextView.setText("");

            textView.setText(String.valueOf(sum));
        }

    }
    //    ------ISLEMLER

    private double getFirstValue() {

        firstValue = Double.valueOf(textView.getText().toString());

        textView.setText("0");

        return firstValue;
    }
}
